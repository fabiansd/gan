import os
import argparse
import sys


import h5py as h5
import numpy as np
from utils.image_generator import image_generator, get_n_samples, load_images
from utils.patch_utils import gen_batch
from utils.logger import plot_generated_batch
from utils.visualize import show_single_image, show_single_pair, visualize_loss_history
from utils import visualize
from utils.logger import inverse_normalization

import matplotlib.pyplot as plt
import time
import h5py
from PIL import Image, ImageFilter

from utils.export import export_current_model
# from utils.freeze_graph import


def test_model(network_path, model_path, input_image_path, target_image_path, save_path_test, name, convert_model):

    # Tests model by uploading a pretrained model, and tests this on the data from given paths. Saves the results
    # in the given folder. Can save both as single image or as (raw - output - target)

    from keras.optimizers import Adam
    from keras.models import load_model

    from keras.preprocessing.image import ImageDataGenerator

    if not os.path.isdir(save_path_test):
        os.mkdir(save_path_test)

    parser = argparse.ArgumentParser(
        description='Script for testing generator models')

    parser = argparse.ArgumentParser(
        description='DCGAN for learning a given image translation')
    parser.add_argument('--gpu', required=True,
                        help='Select which GPU to train on')
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    print('Testing on GPU {}'.format(os.environ['CUDA_VISIBLE_DEVICES']))

    # Load and compile generator model
    generator = load_model(model_path)

    opt_generator = Adam(lr=1E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    generator.compile(loss='mae', optimizer=opt_generator)
    generator.summary()

    # Convert the h5 model to pb, saved in the network folder
    if convert_model:
        print('inputs', generator.input)
        print('Outputs', generator.output)
        output_name = generator.output.name.split(':')[0]
        export_current_model(os.path.join(network_path, name +'.pb'), output_name)

    image_size=generator.layers[0].output_shape[1:3]
    print('Image size is {}'.format(image_size))

    # Load test images
    input_data=load_images(
        input_image_path, image_size, color=False, invert=False)
    target_data=load_images(target_image_path, image_size,
                              color=False, invert=False)


    image_list=os.listdir(input_image_path)

    assert input_data.shape[0] == target_data.shape[0], 'ERROR: number of input and target samples unequal'
    n_samples=input_data.shape[0]

    print('Loaded test data with shape: {}'.format(input_data.shape))

    timerlist=[]

    return image_size


    # Looping through test images
    for index, im in enumerate(input_data[:]):
        input_image=input_data[index:index+1]
        target_image=target_data[index:index+1]

        timing=plot_generated_batch(target_image, input_image, generator,
                                      200, 'data_sr_', index, save_path_test)

        timerlist.append(timing)
        print('Timing iteration {}: {}'.format(index, timing))

    timerlist_np=np.array(timerlist)
    print('Average prediction time: {}'.format(np.mean(timerlist_np)))

    with open(os.path.join(network_path,'timing.txt'), 'a') as f:
        f.write('\nmean time: {}'.format(np.mean(timerlist_np)))
        f.write('\nBatch size: {}'.format(test_batch_size))

def visualize_training(save_path_plot):

    if not os.path.isdir(save_path_plot):
        os.mkdir(save_path_plot)

    visualize.visualize_loss_history(save_path_plot, 0, 200, display=False)
    visualize.visualize_loss_history(
        save_path_plot, 1000, 1200, display=False)
    visualize.visualize_loss_history(save_path_plot, 0, 2000, display=False)


def calculate_L1(target, input_data, data_type, image_size):

    # for i, image in enumerate(target):
    #     show_single_pair(input_data[i], image)
    # exit(0)
    # target = inverse_normalization(target)
    # input_data = inverse_normalization(input_data)

    for i, image in enumerate(target):
        target[i] -= np.median(target[i])
        input_data[i] -= np.median(input_data[i])

        # target = subtract_median_samplewise(target)

    # show_single_image(input_data[8], data_type)
    # return

    diff_mean=np.mean(np.fabs(target.astype(
        'f')-input_data.astype('f')), axis=(1, 2, 3))

    std=np.std(diff_mean)

    mad_score=mad(diff_mean)

    mean_L1=np.mean(diff_mean)

    with open('l1_log.txt', 'a') as f:
        f.write('\n'+data_type + ':{}:{}:{}'.format(mean_L1, std, mad_score))


def calculate_L1_from_wideplot(network_path, image_folder_path, note, image_size):

    # Calculates the maw error with std and mad. The function takes in a path to a folder containing result images
    # generated with "plot batch function". Images must consist of three images concatenated (raw - output - target).
    # The function calculates the l1 between the target and : raw, gaussian and output, respectively

    # for i, image in enumerate(target):
    #     show_single_pair(input_data[i], image)
    # exit(0)

    mean_list=[]

    image_list=os.listdir(image_folder_path)


    # assert(image_list_512 == image_list_256 , "Error, 512 and 256 data paths not matching")
    print('Claculating L1 score of {} images'.format(len(image_list)))

    for i, image in enumerate(image_list):
        image_path=os.path.join(image_folder_path, image)

        im=Image.open(image_path).convert('L')
        im_np=np.array(im)

        raw=im_np[:, 0:image_size[1]]
        output=im_np[:, image_size[1]:image_size[1]*2]
        target=im_np[:, image_size[1]*2:]

        # show_single_image(output)

        raw_im=Image.fromarray(raw)
        gauss=np.array(raw_im.filter(ImageFilter.GaussianBlur(radius=2.5)))


        mean_list.append([mean_diff(raw, target), mean_diff(
            gauss, target), mean_diff(output, target)])

    means=np.array(mean_list)

    total_mean=np.mean(means, axis=0)
    std=np.std(means, axis=0)

    print('raw - gaussian - output')
    print('MAE: {}'.format(total_mean))
    print('Std: {}'.format(std))
    print('Mad: {}'.format(np.apply_along_axis(mad, 0, means)))

    with open(os.path.join(network_path,'MAE_log.txt'), 'a') as f:
        f.write('\n\n {}'.format(note))
        f.write('\nNLLR against: raw - gaussian - output')
        f.write('\nMAE: {}'.format(total_mean))
        f.write('\nStd: {}'.format(std))
        f.write('\nMad: {}'.format(np.apply_along_axis(mad, 0, means)))


def mean_diff(image1, image2):
    return np.mean(np.fabs(image1.astype(
        'f')-image2.astype('f')))


def mad(data):
    return np.mean(np.absolute(data - np.mean(data, axis=0)), axis=0)


def gaussian_smoothing(image_path, save_path, rad=2):

    images=os.listdir(image_path)
    timerlist=[]
    if not os.path.isdir(save_path):
        os.mkdir(save_path)

    for index, image in enumerate(images):
        im_path=os.path.join(image_path, image)

        print(im_path)

        im=Image.open(im_path)

        ts=time.time()

        im_filtered=im.filter(ImageFilter.GaussianBlur(radius=rad))

        ts_delta=time.time()

        timing=ts_delta-ts
        timerlist.append(timing)
        print('Timing iteration {}: {}'.format(index, timing))

        name=(os.path.split(im_path))[-1][0:-4]
        im_filtered.save(os.path.join(
            save_path, name + '.png'))

        # plt.imshow(im_filtered, cmap='gray')
        # plt.show()
    timerlist_np=np.array(timerlist)
    print('Average prediction time: {}'.format(np.mean(timerlist_np)))

# Assumes the following:
# - The dataset contatins two folders named 'rawdata' and 'processed', which is input and target, repectively
# - The network is stored in the 'images for repot' folder
# The results is stored in the model folder, named after the dataset used

def run_all_tests(dict, dataset, gaussian = False, convert_model = False, run_model = True):
    
    # Unwrap dictionary
    name = dict['name']
    network = dict['network']
    model_type = dict['model_type'] 
    model = dict['model']
    
    # Generate necessary paths
    network_path=os.path.join('..', 'images for report', 'generator_results_archive',
                                model_type, network)

    model_path=os.path.join('..', 'images for report', 'generator_results_archive',
                              model_type, network, model)

    save_path=os.path.join('..', 'images for report', 'generator_results_archive',
                                      model_type, network, str(dataset + '_results'))

    input_image_path=os.path.join(
        '..', 'rawdata', dataset, 'rawdata')
    target_image_path=os.path.join(
        '..', 'rawdata', dataset, 'processed')


    # Run the model on the given dataset
    if run_model:
        image_size = test_model(network_path, model_path, input_image_path,
                target_image_path, save_path, name, convert_model)


        # resutlt_data_path = os.path.join(network_path,test_data_folder_for_mae)
        calculate_L1_from_wideplot(
            network_path, save_path, note='running {} data'.format(dataset),image_size=image_size)
    exit(0)
    # Visualize training plots with standard cutscenes
    visualize_loss_history(network_path, 0, 200, display=False)
    visualize_loss_history(network_path, 1800, 2000)
    visualize_loss_history(network_path, 0, 1500)





if __name__ == '__main__':

    dict_256 = {
        'name':'256x256',
        'network':'results_speckle2specklefree_M04-D04_h13-m40-s45',
        'model_type' :'speckle_reduction',
        'model': 'gen_model_epoch_199_speckle2specklefree_M04-D04_h13-m40-s45.h5'
    }

    dict_512 = {
        'name' : '512x512',
        'network' : 'results_speckle2specklefree_M04-D23_h17-m09-s00',
        'model_type' : 'speckle_reduction',
        'model' : 'gen_model_epoch_140_speckle2specklefree.h5',
    }
    dict_slim_512 = {
        'name':'slim 512x512 v1',
        'network':'results_speckle2specklefree_M05-D07_h20-m09-s18',
        'model_type':'speckle_reduction',
        'model':'gen_model_epoch_150_speckle2specklefree.h5'
    }
    dict_reconstruction = {
        'name':'mr reconstruction',
        'network':'results_MR2MR_M05-D08_h18-m31-s02',
        'model_type':'mr_reconstruction',
        'model':'gen_model_epoch_120_MR2MR.h5'
    }
    ditc_slim_512_b2 = {
        'name':'slim 512x512 v2',
        'network':'results_speckle2specklefree_M05-D08_h14-m31-s32',
        'model_type':'speckle_reduction',
        'model':'gen_model_epoch_150_speckle2specklefree.h5'
    }
    dict_US2MR_slim = {
        'name':'slim US2MR',
        'network':'slim US2MR bad results',
        'model_type':'network_save',
        'model':'gen_model_epoch_160_US2MR_distilled.h5'
    }
    dict_US2MR_best = {
        'name':'USMR best',
        'network':'US2MR_best_results_2',
        'model_type':'network_save',
        'model':'gen_weights_epoch_199_US2MR_M03-D23_h19-m03-s28.h5'
    }
    ditc_extreme_slim_512 = {
        'name':'extreme slim 512',
        'network':'results_speckle2specklefree_M05-D23_h10-m24-s42',
        'model_type':'speckle_reduction',
        'model':'gen_model_epoch_150_speckle2specklefree.h5'
    }
    ditc_slim_1024x512 = {
        'name':'extreme slim 512',
        'network':'results_speckle2specklefree_M05-D23_h10-m24-s42',
        'model_type':'speckle_reduction',
        'model':'gen_model_epoch_150_speckle2specklefree.h5'
    }
    dict_US2MR = {
        'name':'US2MR',
        'network':'results_US2MR_M03-D23_h19-m03-s28',
        'model_type':'network_save',
        'model':'model_generator_e200.h5'
    }
    dict_US2MR_distilled = {
        'name':'US2MR',
        'network':'results_US2MR_distilled_M05-D22_h17-m27-s59',
        'model_type':'network_save',
        'model':'gen_model_epoch_200_US2MR_distilled.h5'
    }

    test_batch_size=1
    invert_direction=None
    color=False

    # Dataset for testing the model
    test_dataset = 'a4c_movie_stream'

    run_all_tests(dict_US2MR_distilled, test_dataset, run_model=True)
