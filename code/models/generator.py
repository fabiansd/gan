
from keras.layers import Activation, Input, Dropout, merge, GaussianNoise, Embedding, Flatten, Reshape
from keras.layers.convolutional import Conv2D, UpSampling2D

from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model
from keras.layers.merge import concatenate


def UNETGenerator(input_img_dim, num_output_channels):
    """
    Creates the generator according to the specs in the paper below.
    It's basically a skip layer AutoEncoder

    Generator does the following:
    1. Takes in an image
    2. Generates an image from this image

    Differs from a standard GAN because the image isn't random.
    This model tries to learn a mapping from a suboptimal image to an optimal image.

    [https://arxiv.org/pdf/1611.07004v1.pdf][5. Appendix]
    :param input_img_dim: (channel, height, width)
    :param output_img_dim: (channel, height, width)
    :return:
    """
    # -------------------------------
    # ENCODER
    # C64-C128-C256-C512-C512-C512-C512-C512
    # 1 layer block = Conv - BN - LeakyRelu
    # -------------------------------
    stride = 2

    # batch norm merge axis
    bn_axis = 1

    # Channel mode
    channel_mode = 'channels_last'

    # Input layer
    input_layer = Input(shape=input_img_dim, name="unet_input")

    # Adding Gaussian white noise to image every 2nd layer in the generator

    # 1 encoder C64
    # skip batchnorm on this layer on purpose (from paper)
    en_1 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same', strides=(
        stride, stride), data_format=channel_mode)(input_layer)
    en_1 = LeakyReLU(alpha=0.2)(en_1)

    # 2 encoder C128
    en_2 = Conv2D(filters=128, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_1)
    en_2 = BatchNormalization(name='gen_en_bn_2', axis=bn_axis)(en_2)
    en_2 = LeakyReLU(alpha=0.2)(en_2)

    # 3 encoder C256
    en_3 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_2)
    en_3 = BatchNormalization(name='gen_en_bn_3', axis=bn_axis)(en_3)
    en_3 = LeakyReLU(alpha=0.2)(en_3)

    # 4 encoder C512
    en_4 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_3)
    en_4 = BatchNormalization(name='gen_en_bn_4', axis=bn_axis)(en_4)
    en_4 = LeakyReLU(alpha=0.2)(en_4)

    # 5 encoder C512
    en_5 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_4)
    en_5 = BatchNormalization(name='gen_en_bn_5', axis=bn_axis)(en_5)
    en_5 = LeakyReLU(alpha=0.2)(en_5)

    # 6 encoder C512
    en_6 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_5)
    en_6 = BatchNormalization(name='gen_en_bn_6', axis=bn_axis)(en_6)
    en_6 = LeakyReLU(alpha=0.2)(en_6)

    # 7 encoder C512
    en_7 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_6)
    en_7 = BatchNormalization(name='gen_en_bn_7', axis=bn_axis)(en_7)
    en_7 = LeakyReLU(alpha=0.2)(en_7)

    # 8 encoder C512
    en_8 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_7)
    en_8 = BatchNormalization(name='gen_en_bn_8', axis=bn_axis)(en_8)
    en_8 = LeakyReLU(alpha=0.2)(en_8)

    # -------------------------------
    # DECODER
    # CD512-CD1024-CD1024-C1024-C1024-C512-C256-C128
    # 1 layer block = Conv - Upsample - BN - DO - Relu
    # also adds skip connections (merge). Takes input from previous layer matching encoder layer
    # -------------------------------
    # 1 decoder CD512 (decodes en_8)
    de_1 = UpSampling2D(size=(2, 2), data_format=channel_mode)(en_8)
    de_1 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_1)
    de_1 = BatchNormalization(name='gen_de_bn_1', axis=bn_axis)(de_1)
    de_1 = Dropout(rate=0.5)(de_1)
    de_1 = concatenate([de_1, en_7], axis=3)
    de_1 = Activation('relu')(de_1)

    # 2 decoder CD1024 (decodes en_7)
    de_2 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_1)
    de_2 = Conv2D(filters=1024, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_2)
    de_2 = BatchNormalization(name='gen_de_bn_2', axis=bn_axis)(de_2)
    de_2 = Dropout(rate=0.5)(de_2)
    de_2 = concatenate([de_2, en_6], axis=3)
    de_2 = Activation('relu')(de_2)

    # 3 decoder CD1024 (decodes en_6)
    de_3 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_2)
    de_3 = Conv2D(filters=1024, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_3)
    de_3 = BatchNormalization(name='gen_de_bn_3', axis=bn_axis)(de_3)
    de_3 = Dropout(rate=0.5)(de_3)
    de_3 = concatenate([de_3, en_5], axis=3)
    de_3 = Activation('relu')(de_3)

    # 4 decoder CD1024 (decodes en_5)
    de_4 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_3)
    de_4 = Conv2D(filters=1024, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_4)
    de_4 = BatchNormalization(name='gen_de_bn_4', axis=bn_axis)(de_4)
    de_4 = Dropout(rate=0.5)(de_4)
    de_4 = concatenate([de_4, en_4], axis=3)
    de_4 = Activation('relu')(de_4)

    # 5 decoder CD1024 (decodes en_4)
    de_5 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_4)
    de_5 = Conv2D(filters=1024, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_5)
    de_5 = BatchNormalization(name='gen_de_bn_5', axis=bn_axis)(de_5)
    de_5 = Dropout(rate=0.5)(de_5)
    de_5 = concatenate([de_5, en_3], axis=3)
    de_5 = Activation('relu')(de_5)

    # 6 decoder C512 (decodes en_3)
    de_6 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_5)
    de_6 = Conv2D(filters=512, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_6)
    de_6 = BatchNormalization(name='gen_de_bn_6', axis=bn_axis)(de_6)
    de_6 = Dropout(rate=0.5)(de_6)
    de_6 = concatenate([de_6, en_2], axis=3)
    de_6 = Activation('relu')(de_6)

    # 7 decoder CD256 (decodes en_2)
    de_7 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_6)
    de_7 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_7)
    de_7 = BatchNormalization(name='gen_de_bn_7', axis=bn_axis)(de_7)
    de_7 = Dropout(rate=0.5)(de_7)
    de_7 = concatenate([de_7, en_1], axis=3)
    de_7 = Activation('relu')(de_7)

    # After the last layer in the decoder, a convolution is applied
    # to map to the number of output channels (3 in general,
    # except in colorization, where it is 2), followed by a Tanh
    # function.
    de_8 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_7)
    de_8 = Conv2D(filters=num_output_channels, kernel_size=(
        3, 3), padding='same', data_format=channel_mode)(de_8)
    de_8 = Activation('tanh')(de_8)

    unet_generator = Model(name='unet_generator', inputs=[
                           input_layer], outputs=[de_8])
    return unet_generator


def slim_UNETGenerator(input_img_dim, num_output_channels):
    """
    Creates the generator according to the specs in the paper below.
    It's basically a skip layer AutoEncoder

    Generator does the following:
    1. Takes in an image
    2. Generates an image from this image

    Differs from a standard GAN because the image isn't random.
    This model tries to learn a mapping from a suboptimal image to an optimal image.

    [https://arxiv.org/pdf/1611.07004v1.pdf][5. Appendix]
    :param input_img_dim: (channel, height, width)
    :param output_img_dim: (channel, height, width)
    :return:
    """
    # -------------------------------
    # ENCODER
    # C64-C128-C256-C512-C512-C512-C512-C512
    # 1 layer block = Conv - BN - LeakyRelu
    # -------------------------------
    stride = 2

    # batch norm merge axis
    bn_axis = 1

    # Channel mode
    channel_mode = 'channels_last'

    # Input layer
    input_layer = Input(shape=input_img_dim, name="unet_input")

    # 1 encoder C64
    # skip batchnorm on this layer on purpose (from paper)
    en_1 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same', strides=(
        stride, stride), data_format=channel_mode)(input_layer)
    en_1 = LeakyReLU(alpha=0.2)(en_1)

    # 2 encoder C128
    en_2 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_1)
    en_2 = BatchNormalization(name='gen_en_bn_2')(en_2)
    en_2 = LeakyReLU(alpha=0.2)(en_2)

    # 3 encoder C256
    en_3 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_2)
    en_3 = BatchNormalization(name='gen_en_bn_3')(en_3)
    en_3 = LeakyReLU(alpha=0.2)(en_3)

    # 4 encoder C512
    en_4 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_3)
    en_4 = BatchNormalization(name='gen_en_bn_4')(en_4)
    en_4 = LeakyReLU(alpha=0.2)(en_4)

    # 5 encoder C512
    en_5 = Conv2D(filters=128, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_4)
    en_5 = BatchNormalization(name='gen_en_bn_5')(en_5)
    en_5 = LeakyReLU(alpha=0.2)(en_5)

    # 6 encoder C512
    en_6 = Conv2D(filters=128, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_5)
    en_6 = BatchNormalization(name='gen_en_bn_6')(en_6)
    en_6 = LeakyReLU(alpha=0.2)(en_6)

    # 7 encoder C512
    en_7 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_6)
    en_7 = BatchNormalization(name='gen_en_bn_7')(en_7)
    en_7 = LeakyReLU(alpha=0.2)(en_7)

    # 8 encoder C512
    en_8 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_7)
    en_8 = BatchNormalization(name='gen_en_bn_8')(en_8)
    en_8 = LeakyReLU(alpha=0.2)(en_8)

    # -------------------------------
    # DECODER
    # CD512-CD1024-CD1024-C1024-C1024-C512-C256-C128
    # 1 layer block = Conv - Upsample - BN - DO - Relu
    # also adds skip connections (merge). Takes input from previous layer matching encoder layer
    # -------------------------------
    # 1 decoder CD512 (decodes en_8)
    de_1 = UpSampling2D(size=(2, 2), data_format=channel_mode)(en_8)
    de_1 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_1)
    de_1 = BatchNormalization(name='gen_de_bn_1')(de_1)
    de_1 = Dropout(rate=0.5)(de_1)
    de_1 = concatenate([de_1, en_7], axis=3)
    de_1 = Activation('relu')(de_1)

    # 2 decoder CD1024 (decodes en_7)
    de_2 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_1)
    de_2 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_2)
    de_2 = BatchNormalization(name='gen_de_bn_2')(de_2)
    de_2 = Dropout(rate=0.5)(de_2)
    de_2 = concatenate([de_2, en_6], axis=3)
    de_2 = Activation('relu')(de_2)

    # 3 decoder CD1024 (decodes en_6)
    de_3 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_2)
    de_3 = Conv2D(filters=256, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_3)
    de_3 = BatchNormalization(name='gen_de_bn_3')(de_3)
    de_3 = Dropout(rate=0.5)(de_3)
    de_3 = concatenate([de_3, en_5], axis=3)
    de_3 = Activation('relu')(de_3)

    # 4 decoder CD1024 (decodes en_5)
    de_4 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_3)
    de_4 = Conv2D(filters=128, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_4)
    de_4 = BatchNormalization(name='gen_de_bn_4')(de_4)
    de_4 = Dropout(rate=0.5)(de_4)
    de_4 = concatenate([de_4, en_4], axis=3)
    de_4 = Activation('relu')(de_4)

    # 5 decoder CD1024 (decodes en_4)
    de_5 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_4)
    de_5 = Conv2D(filters=128, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_5)
    de_5 = BatchNormalization(name='gen_de_bn_5')(de_5)
    de_5 = Dropout(rate=0.5)(de_5)
    de_5 = concatenate([de_5, en_3], axis=3)
    de_5 = Activation('relu')(de_5)

    # 6 decoder C512 (decodes en_3)
    de_6 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_5)
    de_6 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_6)
    de_6 = BatchNormalization(name='gen_de_bn_6')(de_6)
    de_6 = Dropout(rate=0.5)(de_6)
    de_6 = concatenate([de_6, en_2], axis=3)
    de_6 = Activation('relu')(de_6)

    # 7 decoder CD256 (decodes en_2)
    de_7 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_6)
    de_7 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_7)
    de_7 = BatchNormalization(name='gen_de_bn_7')(de_7)
    de_7 = Dropout(rate=0.5)(de_7)
    de_7 = concatenate([de_7, en_1], axis=3)
    de_7 = Activation('relu')(de_7)

    # After the last layer in the decoder, a convolution is applied
    # to map to the number of output channels (3 in general,
    # except in colorization, where it is 2), followed by a Tanh
    # function.
    de_8 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_7)
    de_8 = Conv2D(filters=num_output_channels, kernel_size=(
        3, 3), padding='same', data_format=channel_mode)(de_8)
    de_8 = Activation('tanh')(de_8)

    unet_generator = Model(name='unet_generator', inputs=[
                           input_layer], outputs=[de_8])
    return unet_generator


def super_slim_UNETGenerator(input_img_dim, num_output_channels):
    """
    Creates the generator according to the specs in the paper below.
    It's basically a skip layer AutoEncoder

    Generator does the following:
    1. Takes in an image
    2. Generates an image from this image

    Differs from a standard GAN because the image isn't random.
    This model tries to learn a mapping from a suboptimal image to an optimal image.

    [https://arxiv.org/pdf/1611.07004v1.pdf][5. Appendix]
    :param input_img_dim: (channel, height, width)
    :param output_img_dim: (channel, height, width)
    :return:
    """
    # -------------------------------
    # ENCODER
    # C64-C128-C256-C512-C512-C512-C512-C512
    # 1 layer block = Conv - BN - LeakyRelu
    # -------------------------------
    stride = 2

    # batch norm merge axis
    bn_axis = 1

    # Channel mode
    channel_mode = 'channels_last'

    # Input layer
    input_layer = Input(shape=input_img_dim, name="unet_input")

    # 1 encoder C64
    # skip batchnorm on this layer on purpose (from paper)
    en_1 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same', strides=(
        stride, stride), data_format=channel_mode)(input_layer)
    en_1 = LeakyReLU(alpha=0.2)(en_1)

    # 2 encoder C128
    en_2 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_1)
    en_2 = BatchNormalization(name='gen_en_bn_2')(en_2)
    en_2 = LeakyReLU(alpha=0.2)(en_2)

    # 3 encoder C256
    en_3 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_2)
    en_3 = BatchNormalization(name='gen_en_bn_3')(en_3)
    en_3 = LeakyReLU(alpha=0.2)(en_3)

    # 4 encoder C512
    en_4 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_3)
    en_4 = BatchNormalization(name='gen_en_bn_4')(en_4)
    en_4 = LeakyReLU(alpha=0.2)(en_4)

    # 5 encoder C512
    en_5 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_4)
    en_5 = BatchNormalization(name='gen_en_bn_5')(en_5)
    en_5 = LeakyReLU(alpha=0.2)(en_5)

    # 6 encoder C512
    en_6 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_5)
    en_6 = BatchNormalization(name='gen_en_bn_6')(en_6)
    en_6 = LeakyReLU(alpha=0.2)(en_6)

    # 7 encoder C512
    en_7 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_6)
    en_7 = BatchNormalization(name='gen_en_bn_7')(en_7)
    en_7 = LeakyReLU(alpha=0.2)(en_7)

    # 8 encoder C512
    en_8 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_7)
    en_8 = BatchNormalization(name='gen_en_bn_8')(en_8)
    en_8 = LeakyReLU(alpha=0.2)(en_8)

    # -------------------------------
    # DECODER
    # CD512-CD1024-CD1024-C1024-C1024-C512-C256-C128
    # 1 layer block = Conv - Upsample - BN - DO - Relu
    # also adds skip connections (merge). Takes input from previous layer matching encoder layer
    # -------------------------------
    # 1 decoder CD512 (decodes en_8)
    de_1 = UpSampling2D(size=(2, 2), data_format=channel_mode)(en_8)
    de_1 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_1)
    de_1 = BatchNormalization(name='gen_de_bn_1')(de_1)
    de_1 = Dropout(rate=0.5)(de_1)
    de_1 = concatenate([de_1, en_7], axis=3)
    de_1 = Activation('relu')(de_1)

    # 2 decoder CD1024 (decodes en_7)
    de_2 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_1)
    de_2 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_2)
    de_2 = BatchNormalization(name='gen_de_bn_2')(de_2)
    de_2 = Dropout(rate=0.5)(de_2)
    de_2 = concatenate([de_2, en_6], axis=3)
    de_2 = Activation('relu')(de_2)

    # 3 decoder CD1024 (decodes en_6)
    de_3 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_2)
    de_3 = Conv2D(filters=64, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_3)
    de_3 = BatchNormalization(name='gen_de_bn_3')(de_3)
    de_3 = Dropout(rate=0.5)(de_3)
    de_3 = concatenate([de_3, en_5], axis=3)
    de_3 = Activation('relu')(de_3)

    # 4 decoder CD1024 (decodes en_5)
    de_4 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_3)
    de_4 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_4)
    de_4 = BatchNormalization(name='gen_de_bn_4')(de_4)
    de_4 = Dropout(rate=0.5)(de_4)
    de_4 = concatenate([de_4, en_4], axis=3)
    de_4 = Activation('relu')(de_4)

    # 5 decoder CD1024 (decodes en_4)
    de_5 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_4)
    de_5 = Conv2D(filters=32, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_5)
    de_5 = BatchNormalization(name='gen_de_bn_5')(de_5)
    de_5 = Dropout(rate=0.5)(de_5)
    de_5 = concatenate([de_5, en_3], axis=3)
    de_5 = Activation('relu')(de_5)

    # 6 decoder C512 (decodes en_3)
    de_6 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_5)
    de_6 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_6)
    de_6 = BatchNormalization(name='gen_de_bn_6')(de_6)
    de_6 = Dropout(rate=0.5)(de_6)
    de_6 = concatenate([de_6, en_2], axis=3)
    de_6 = Activation('relu')(de_6)

    # 7 decoder CD256 (decodes en_2)
    de_7 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_6)
    de_7 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_7)
    de_7 = BatchNormalization(name='gen_de_bn_7')(de_7)
    de_7 = Dropout(rate=0.5)(de_7)
    de_7 = concatenate([de_7, en_1], axis=3)
    de_7 = Activation('relu')(de_7)

    # After the last layer in the decoder, a convolution is applied
    # to map to the number of output channels (3 in general,
    # except in colorization, where it is 2), followed by a Tanh
    # function.
    de_8 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_7)
    de_8 = Conv2D(filters=num_output_channels, kernel_size=(
        3, 3), padding='same', data_format=channel_mode)(de_8)
    de_8 = Activation('tanh')(de_8)

    unet_generator = Model(name='unet_generator', inputs=[
                           input_layer], outputs=[de_8])
    return unet_generator


def extreme_slim_UNETGenerator(input_img_dim, num_output_channels):
    """
    Creates the generator according to the specs in the paper below.
    It's basically a skip layer AutoEncoder

    Generator does the following:
    1. Takes in an image
    2. Generates an image from this image

    Differs from a standard GAN because the image isn't random.
    This model tries to learn a mapping from a suboptimal image to an optimal image.

    [https://arxiv.org/pdf/1611.07004v1.pdf][5. Appendix]
    :param input_img_dim: (channel, height, width)
    :param output_img_dim: (channel, height, width)
    :return:
    """
    # -------------------------------
    # ENCODER
    # C64-C128-C256-C512-C512-C512-C512-C512
    # 1 layer block = Conv - BN - LeakyRelu
    # -------------------------------
    stride = 2

    # batch norm merge axis
    bn_axis = 1

    # Channel mode
    channel_mode = 'channels_last'

    # Input layer
    input_layer = Input(shape=input_img_dim, name="unet_input")

    # 1 encoder C64
    # skip batchnorm on this layer on purpose (from paper)
    en_1 = Conv2D(filters=2, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same', strides=(
        stride, stride), data_format=channel_mode)(input_layer)
    en_1 = LeakyReLU(alpha=0.2)(en_1)

    # 2 encoder C122
    en_2 = Conv2D(filters=2, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_1)
    en_2 = BatchNormalization(name='gen_en_bn_2')(en_2)
    en_2 = LeakyReLU(alpha=0.2)(en_2)

    # 3 encoder C256
    en_3 = Conv2D(filters=4, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_2)
    en_3 = BatchNormalization(name='gen_en_bn_3')(en_3)
    en_3 = LeakyReLU(alpha=0.2)(en_3)

    # 4 encoder C512
    en_4 = Conv2D(filters=4, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_3)
    en_4 = BatchNormalization(name='gen_en_bn_4')(en_4)
    en_4 = LeakyReLU(alpha=0.2)(en_4)

    # 5 encoder C512
    en_5 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_4)
    en_5 = BatchNormalization(name='gen_en_bn_5')(en_5)
    en_5 = LeakyReLU(alpha=0.2)(en_5)

    # 6 encoder C512
    en_6 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_5)
    en_6 = BatchNormalization(name='gen_en_bn_6')(en_6)
    en_6 = LeakyReLU(alpha=0.2)(en_6)

    # 7 encoder C512
    en_7 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_6)
    en_7 = BatchNormalization(name='gen_en_bn_7')(en_7)
    en_7 = LeakyReLU(alpha=0.2)(en_7)

    # 8 encoder C512
    en_8 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal', padding='same',
                  strides=(stride, stride), data_format=channel_mode)(en_7)
    en_8 = BatchNormalization(name='gen_en_bn_8')(en_8)
    en_8 = LeakyReLU(alpha=0.2)(en_8)

    # -------------------------------
    # DECODER
    # CD512-CD1024-CD1024-C1024-C1024-C512-C256-C128
    # 1 layer block = Conv - Upsample - BN - DO - Relu
    # also adds skip connections (merge). Takes input from previous layer matching encoder layer
    # -------------------------------
    # 1 decoder CD512 (decodes en_8)
    de_1 = UpSampling2D(size=(2, 2), data_format=channel_mode)(en_8)
    de_1 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_1)
    de_1 = BatchNormalization(name='gen_de_bn_1')(de_1)
    de_1 = Dropout(rate=0.5)(de_1)
    de_1 = concatenate([de_1, en_7], axis=3)
    de_1 = Activation('relu')(de_1)

    # 2 decoder CD1024 (decodes en_7)
    de_2 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_1)
    de_2 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_2)
    de_2 = BatchNormalization(name='gen_de_bn_2')(de_2)
    de_2 = Dropout(rate=0.5)(de_2)
    de_2 = concatenate([de_2, en_6], axis=3)
    de_2 = Activation('relu')(de_2)

    # 3 decoder CD1024 (decodes en_6)
    de_3 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_2)
    de_3 = Conv2D(filters=16, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_3)
    de_3 = BatchNormalization(name='gen_de_bn_3')(de_3)
    de_3 = Dropout(rate=0.5)(de_3)
    de_3 = concatenate([de_3, en_5], axis=3)
    de_3 = Activation('relu')(de_3)

    # 4 decoder CD1024 (decodes en_5)
    de_4 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_3)
    de_4 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_4)
    de_4 = BatchNormalization(name='gen_de_bn_4')(de_4)
    de_4 = Dropout(rate=0.5)(de_4)
    de_4 = concatenate([de_4, en_4], axis=3)
    de_4 = Activation('relu')(de_4)

    # 5 decoder CD1024 (decodes en_4)
    de_5 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_4)
    de_5 = Conv2D(filters=8, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_5)
    de_5 = BatchNormalization(name='gen_de_bn_5')(de_5)
    de_5 = Dropout(rate=0.5)(de_5)
    de_5 = concatenate([de_5, en_3], axis=3)
    de_5 = Activation('relu')(de_5)

    # 6 decoder C512 (decodes en_3)
    de_6 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_5)
    de_6 = Conv2D(filters=4, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_6)
    de_6 = BatchNormalization(name='gen_de_bn_6')(de_6)
    de_6 = Dropout(rate=0.5)(de_6)
    de_6 = concatenate([de_6, en_2], axis=3)
    de_6 = Activation('relu')(de_6)

    # 7 decoder CD256 (decodes en_2)
    de_7 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_6)
    de_7 = Conv2D(filters=4, kernel_size=(3, 3), kernel_initializer='he_normal',
                  padding='same', data_format=channel_mode)(de_7)
    de_7 = BatchNormalization(name='gen_de_bn_7')(de_7)
    de_7 = Dropout(rate=0.5)(de_7)
    de_7 = concatenate([de_7, en_1], axis=3)
    de_7 = Activation('relu')(de_7)

    # After the last layer in the decoder, a convolution is applied
    # to map to the number of output channels (3 in general,
    # except in colorization, where it is 2), followed by a Tanh
    # function.
    de_8 = UpSampling2D(size=(2, 2), data_format=channel_mode)(de_7)
    de_8 = Conv2D(filters=num_output_channels, kernel_size=(
        3, 3), padding='same', data_format=channel_mode)(de_8)
    de_8 = Activation('tanh')(de_8)

    unet_generator = Model(name='unet_generator', inputs=[
                           input_layer], outputs=[de_8])
    return unet_generator
