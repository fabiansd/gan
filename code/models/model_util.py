
import keras.backend as K


def WGAN_loss(y_true, y_pred):
    return K.mean(y_true * y_pred)
