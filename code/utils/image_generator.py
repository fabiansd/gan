import os
import sys
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import scipy.misc


from utils.helpfunctions import shuffle_arrays_equally, normalize, invert_image
from utils.visualize import show_single_pair


def get_n_samples(data_dir_name, data_type, batch_size):

    org_images, targ_images, original_image_dir, target_image_dir = get_images_lists(
        data_dir_name, data_type)

    return len(org_images) - len(org_images) % batch_size, len(targ_images) - len(targ_images) % batch_size


def get_images_lists(data_dir_name, data_type):

    # data_dir_name = os.path.join(data_dir_name,direction )
    # data_folders = os.listdir(data_dir_name)

    # data_folders = data_folders[switch_datatype(data_type)]
    data_folders = switch_datatype(data_type)

    original_image_dir = os.path.join(data_dir_name, data_folders[0])
    target_image_dir = os.path.join(data_dir_name, data_folders[1])

    org_images = os.listdir(original_image_dir)
    targ_images = os.listdir(target_image_dir)

    return org_images, targ_images, original_image_dir, target_image_dir


def image_generator(data_dir, data_type, im_size, batch_size, invert_direction=None, random_seed=666, color=True):

    org_images, targ_images, original_image_dir, target_image_dir = get_images_lists(
        data_dir, data_type)

    n_samples = len(org_images)

    # Test that the data is consistent in number
    assert len(org_images) == len(targ_images), 'Mismatch in number of original and target ' + \
        data_type + \
        ' images: {} and {}'.format(len(org_images), len(targ_images))

    if invert_direction == 'US2MR':
        org_inv = False
        targ_inv = True
    elif invert_direction == 'MR2US':
        org_inv = True
        targ_inv = False
    else:
        org_inv = False
        targ_inv = False
        # Loading images to numpy array on the form NHWC
    original_data = load_images(
        original_image_dir, im_size, color=color, invert=org_inv)
    target_data = load_images(
        target_image_dir, im_size, color=color, invert=targ_inv)

    # Shuffle original and target data equally according to given seed
    original_data, target_data = shuffle_arrays_equally(
        original_data, target_data, random_seed)

    # Clip of the images making the number of samples undivisible by batch size
    n_samples -= (n_samples % batch_size)

    original_data = original_data[:n_samples]
    target_data = target_data[:n_samples]

    print('Loaded ' + data_type + ' data and target data with shapes {} and {}, respectively\n\n'.format(
        original_data.shape, target_data.shape))

    # Yield data in an infinite loop
    while True:

        for i in range(0, n_samples, batch_size):
            i_end = i + batch_size
            original_batch = original_data[i:i_end, :, :, :]
            target_batch = target_data[i:i_end, :, :, :]

            '''

            if i_end <= n_samples:
                original_batch = original_data[i:i_end, :, :, :]
                target_batch = target_data[i:i_end, :, :, :]

            # maybe just exclude these if the network doesnt accept different batch sizes..
            else:
                original_batch = original_data[i_end-batch_size:i_end, :, :, :]
                target_batch = target_data[i_end-batch_size:i_end, :, :, :]
            '''
            yield original_batch, target_batch


def load_images(path, im_size, color, invert=False):
    images = os.listdir(path)

    channels = 1
    mode = ''

    N = len(images)
    if color:
        channels = 3
        mode = 'RGB'
    else:
        channels = 1
        mode = 'L'

    imagelist = np.ndarray(
        shape=(N, im_size[0], im_size[1], channels), dtype='float64')

    first = True

    for i, image_name in enumerate(images):
        # Reading image from folder

        temp_im = scipy.misc.imread(os.path.join(path, image_name), mode=mode)

        # Works with antialiasing
        # temp_im = Image.imread(os.path.join(path,image_name), mode=mode)

        if first:
            # print('Resizing images from {} to {}'.format(temp_im.shape,(im_size[0],im_size[1],channels)))
            first = False

        # Resizing images to desired image size
        temp_im = scipy.misc.imresize(
            temp_im, im_size, interp='bicubic', mode=mode)
        # temp_im = temp_im.resize(im_size, Image.ANTIALIAS)

        # Resize to keep 3rd dimension of color = False
        if mode == 'L':
            temp_im_array = np.array(temp_im).reshape(
                (im_size[0], im_size[1], 1))
        else:
            temp_im_array = np.array(temp_im)

        # Roll the axes to get 'channel_first' configuration
        # temp_im_array = np.rollaxis(temp_im_array, 2, 0)

        # Save images in one ndarray
        imagelist[i, :, :, :] = temp_im_array

    # Invert mr image
    if invert:
        imagelist = invert_image(imagelist)

    # Normalize images between -1 and 1
    imagelist = normalize(imagelist)

    return imagelist


def switch_datatype(type):
    return {
        'training': ['train_org', 'train_targ'],
        'validating': ['valid_org', 'valid_targ'],
        'testing': ['test_org', 'test_targ']
    }[type]


if __name__ == '__main__':
    datapath = os.path.join('..', '..', 'facade_data')
    batch_size = 1

    direction = 'MR2US'

    org_samples_training, targ_samples_training = get_n_samples(
        datapath, 'training')
    org_samples_validating, targ_samples_validating = get_n_samples(
        datapath, 'validating')
    org_samples_testing, targ_samples_testing = get_n_samples(
        datapath, 'testing')

    for epoch in range(5):
        print('Epoch: {}\n'.format(epoch))
        train_gen = image_generator(
            datapath, 'training', (256, 256), batch_size, direction)
        valid_gen = image_generator(
            datapath, 'validating', (256, 256), batch_size, direction)
        test_gen = image_generator(
            datapath, 'testing', (256, 256), batch_size, direction)
    # HERE
        print()
        for i in range(0, org_samples_training, batch_size):
            Org_train, targ_train = next(train_gen)
            print('i: {}'.format(i))
            print(Org_train.shape)
            # print(targ_train.shape)
        print()
        for i in range(0, org_samples_validating, batch_size):
            Org_train, targ_train = next(valid_gen)
            print('i: {}'.format(i))
            print(Org_train.shape)
            # print(targ_train.shape)
        print()
        for i in range(0, org_samples_testing, batch_size):
            Org_train, targ_train = next(test_gen)
            print('i: {}'.format(i))
            print(Org_train.shape)
            # print(targ_train.shape)

        i = input()
