import numpy as np
from threading import Timer


def normalize(inarr):

    return ((inarr - 127.5) / 127.5).astype('float64')


def denormalize(inarr):

    return ((inarr * 127.5) + 127.5).astype('uint8')


def invert_image(inarr):
    return np.abs(inarr - 255)


def generate_random_index(n_org_samples, seed=47):

    np.random.seed(seed=seed)
    random_index = np.arange(n_org_samples)
    np.random.shuffle(random_index)

    return random_index


def shuffle_arrays_equally(a, b, seed):
    # fix the seed
    np.random.seed(seed=seed)
    # Generate the permutation index array.
    permutation = np.random.permutation(a.shape[0])
    # Shuffle the arrays by giving the permutation in the square brackets.
    shuffled_a = a[permutation]
    shuffled_b = b[permutation]
    return shuffled_a, shuffled_b


# class Watchdog:
#     def __init__(self, timeout, userHandler=None):  # timeout in seconds
#         self.timeout = timeout
#         self.handler = userHandler if userHandler is not None else self.defaultHandler
#         self.timer = Timer(self.timeout, self.handler)
#         self.timer.start()

#     def reset(self):
#         self.timer.cancel()
#         self.timer = Timer(self.timeout, self.handler)
#         self.timer.start()

#     def stop(self):
#         self.timer.cancel()

#     def defaultHandler(self):
#         raise self
