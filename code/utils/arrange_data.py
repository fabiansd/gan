import os
import numpy as np
from shutil import copyfile
import shutil

# Data is demanded to be separated into two folders, namely original and target
# The images pairs must have the same names such that they get matched

# The data is saved in 6 folders, 2 folder pairs for training, validation and testing
# The data is saved on the path:
# data/direction_organized_data

dir_dict = {'topdown': [0, 1],
            'bottomup': [1, 0],
            'topitself': [0, 0],
            'bottomitself': [1, 1]}


def organize_data(data_dir_name, validation_split, test_split, direction, dataname, seed=666):

    # Reading image paths

    org_targ_folders = os.listdir(data_dir_name)

    original_data_dir = os.path.join(
        data_dir_name, org_targ_folders[dir_dict[direction][0]])

    target_data_dir = os.path.join(
        data_dir_name, org_targ_folders[dir_dict[direction][1]])

    org_image_list = os.listdir(original_data_dir)
    targ_image_list = os.listdir(target_data_dir)

    n_org_samples = len(org_image_list)
    n_targ_samples = len(targ_image_list)
    assert n_org_samples == n_targ_samples, 'Mismatch between number of original and target samples: {} and {}'.format(
        n_org_samples, n_targ_samples)

    # Making folders for data, organized in 3 separate folder for training, validation and testing
    # both for original and target images
    splitted_data_base = os.path.join('..', '..', 'data', dataname)

    remake_dir(splitted_data_base)

    # Compress??
    org_train = os.path.join(splitted_data_base, 'train_org')
    targ_train = os.path.join(splitted_data_base, 'train_targ')
    org_valid = os.path.join(splitted_data_base, 'valid_org')
    targ_valid = os.path.join(splitted_data_base, 'valid_targ')
    org_test = os.path.join(splitted_data_base, 'test_org')
    targ_test = os.path.join(splitted_data_base, 'test_targ')

    make_dir(org_train)
    make_dir(targ_train)
    make_dir(org_valid)
    make_dir(targ_valid)
    make_dir(org_test)
    make_dir(targ_test)

    # random_index = generate_random_index(n_org_samples, seed=seed)

    # Copying files into the new structure in a non shuffled manner, may be neccessary to only move instead
    for index in range(n_org_samples):

        if index < validation_split:  # int(validation_split * n_org_samples):
            copyfile(os.path.join(original_data_dir, org_image_list[index]), os.path.join(
                org_train, org_image_list[index]))
            copyfile(os.path.join(target_data_dir, targ_image_list[index]), os.path.join(
                targ_train, targ_image_list[index]))

        elif index < test_split:  # int(test_split * n_org_samples):
            copyfile(os.path.join(original_data_dir, org_image_list[index]), os.path.join(
                org_valid, org_image_list[index]))
            copyfile(os.path.join(target_data_dir, targ_image_list[index]), os.path.join(
                targ_valid, targ_image_list[index]))

        else:
            copyfile(os.path.join(original_data_dir, org_image_list[index]), os.path.join(
                org_test, org_image_list[index]))
            copyfile(os.path.join(target_data_dir, targ_image_list[index]), os.path.join(
                targ_test, targ_image_list[index]))


def remake_dir(dir_name):

    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)

    os.mkdir(dir_name)


def make_dir(dir_name):

    if not os.path.isdir(dir_name):

        os.mkdir(dir_name)


if __name__ == '__main__':
    from helpfunctions import generate_random_index
    direction = 'bottomup'
    # dataname = direction + '_original_mrus_data'
    dataname = 'US2MR_distilled'
    datapath = os.path.join('..', '..', 'data', 'datapairs_png_distilled')

    # Split for the original data
    mr_us_val_split = 564
    mr_us_test_split = 600

    # Split for distilled data
    mr_us_distilled_val_split = 466
    mr_us_distilled_test_split = 488

    despeckling_val_test_split = (520, 579)

    organize_data(datapath, validation_split=mr_us_distilled_val_split, test_split=mr_us_distilled_test_split, direction=direction,
                  dataname=dataname)

else:
    from utils.helpfunctions import generate_random_index
