import h5py
import numpy as np
import os
from PIL import Image
import sys
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from skimage.transform import rotate, rescale
import math
import scipy

from natsort import natsorted


def f2i(arr):
    return (arr * 255).astype('uint8')


def i2f(arr):
    return arr.astype('float64') / 255


def get_big_MR(fmr):
    ed_frame = fmr['ed_frame'].value
    es_frame = fmr['es_frame'].value

    data_full = fmr['data_full'].value

    data_ed_big = data_full[ed_frame, :, :, 0]
    data_es_big = data_full[es_frame, :, :, 0]

    return data_ed_big, data_es_big


def rotate_point(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    angle = deg2rad(angle)

    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


def rotate_landmarks(landmarks, origin, angle):

    for i, point in enumerate(landmarks):

        landmarks[i] = rotate_point(origin, point, angle)
    return landmarks


def align_US_and_MR(fmr, fus):

    # Get rotation data for transformation
    ed_angle_mr, es_angle_mr, ed_origin_mr, es_origin_mr = find_h_axis_rotation(
        fmr)
    ed_angle_us, es_angle_us, ed_origin_us, es_origin_us = find_h_axis_rotation(
        fus)

    # Get landmarks
    label_ed_MR = fmr['label_ed'].value
    label_es_MR = fmr['label_es'].value
    crop_corner = fmr['crop_corner'].value

    label_ed_MR[:, 0] += crop_corner[0]
    label_ed_MR[:, 1] += crop_corner[1]

    label_es_MR[:, 0] += crop_corner[0]
    label_es_MR[:, 1] += crop_corner[1]

    label_ed_US = fus['label_ed'].value
    label_es_US = fus['label_es'].value

    # Get numpy image pixel data
    data_ed_MR, data_es_MR = get_big_MR(fmr)
    data_ed_US, data_es_US = fus['data_ed'].value, fus['data_es'].value

    print('Original shapes (ed_MR, es_MR, ed_US, es_US) : {} {} {} {}'.format(
        data_ed_MR.shape, data_es_MR.shape, data_ed_US.shape, data_es_US.shape))

    org_ed_MR_shape, org_es_MR_shape = data_ed_MR.shape, data_es_MR.shape

    plot_alignment(data_ed_MR, data_ed_US, label_ed_MR,
                   label_ed_US, fus['view'].value)

    # Scale images to equal spacing
    # Reszise images to same size according to pixel spacing
    mr_spacing = fmr['spacing'].value
    us_spacing = fus['spacing'].value

    MR_re_scale = mr_spacing / us_spacing
    MR_new_size = org_ed_MR_shape * MR_re_scale
    MR_new_size = [int(MR_new_size[0]), int(MR_new_size[1])]

    label_ed_MR *= MR_re_scale
    ed_origin_mr = label_ed_MR[1]

    label_es_MR *= MR_re_scale
    es_origin_mr = label_es_MR[1]

    ed_origin_us = label_ed_US[1]
    es_origin_us = label_es_US[1]

    data_ed_MR = scipy.misc.imresize(
        data_ed_MR, size=MR_new_size, interp='bicubic')
    data_es_MR = scipy.misc.imresize(
        data_es_MR, size=MR_new_size, interp='bicubic')

    plot_alignment(data_ed_MR, data_ed_US, label_ed_MR,
                   label_ed_US, fus['view'].value)

    # Pad MR image with black
    pad_width = 500

    data_ed_MR = np.pad(data_ed_MR, pad_width=pad_width, mode='constant')
    label_ed_MR += pad_width

    data_es_MR = np.pad(data_es_MR, pad_width=pad_width, mode='constant')
    label_es_MR += pad_width

    # Pad US image with black
    data_ed_US = np.pad(data_ed_US, pad_width=pad_width, mode='constant')
    label_ed_US += pad_width

    data_es_US = np.pad(data_es_US, pad_width=pad_width, mode='constant')
    label_es_US += pad_width

    plot_alignment(data_ed_MR, data_ed_US, label_ed_MR,
                   label_ed_US, fus['view'].value)

    # Rotate images
    data_ed_MR = rotate(data_ed_MR, ed_angle_mr, center=ed_origin_mr[::-1])
    data_ed_US = rotate(data_ed_US, ed_angle_us, center=ed_origin_us[::-1])

    data_es_MR = rotate(data_es_MR, es_angle_mr, center=es_origin_mr[::-1])
    data_es_US = rotate(data_es_US, es_angle_us, center=es_origin_us[::-1])

    # Rotate landmarks
    label_ed_MR = rotate_landmarks(label_ed_MR, ed_origin_mr, ed_angle_mr)
    label_es_MR = rotate_landmarks(label_es_MR, es_origin_mr, es_angle_mr)

    label_ed_US = rotate_landmarks(label_ed_US, ed_origin_us, ed_angle_us)
    label_es_US = rotate_landmarks(label_es_US, es_origin_us, es_angle_us)

    print('rotated')
    plot_alignment(data_ed_MR, data_ed_US, label_ed_MR,
                   label_ed_US, fus['view'].value)

    # #Scaling the image size and landmarks by scaling us down to mr size
    # US_down_scale = us_spacing / mr_spacing
    # US_new_size = data_ed_US.shape * US_down_scale

    # US_new_size = [int(US_new_size[0]),int(US_new_size[1])]

    # label_ed_US *= US_down_scale
    # ed_origin_us = label_ed_US[1]

    # label_es_US *= US_down_scale
    # es_origin_us = label_es_US[1]

    # data_ed_US = scipy.misc.imresize(data_ed_US, size = US_new_size, interp='bicubic')
    # data_es_US = scipy.misc.imresize(data_es_US, size = US_new_size, interp='bicubic')

    # Tuning for where to place the crop
    above_or = 40
    below_or = 500
    width = 200

    # print('Cropping ed_MR x : {} to {} '.format(int(ed_origin_mr[0]) - above_or, int(ed_origin_mr[0]) + below_or))
    # print('Cropping es_MR x : {} to {} '.format(int(es_origin_mr[0]) - above_or, int(es_origin_mr[0]) + below_or))
    # print('Cropping ed_US x : {} to {} '.format(int(ed_origin_us[0]) - above_or, int(ed_origin_us[0]) + below_or))
    # print('Cropping es_US x : {} to {} '.format(int(es_origin_us[0]) - above_or, int(es_origin_us[0]) + below_or))
    # Cropping the images to be of same size

    data_ed_MR = data_ed_MR[int(ed_origin_mr[0]) - above_or:int(ed_origin_mr[0]) + below_or,
                            int(ed_origin_mr[1]) - width:int(ed_origin_mr[1]) + width]

    data_ed_US = data_ed_US[int(ed_origin_us[0]) - above_or:int(ed_origin_us[0]) + below_or,
                            int(ed_origin_us[1]) - width:int(ed_origin_us[1]) + width]

    data_es_MR = data_es_MR[int(es_origin_mr[0]) - above_or:int(es_origin_mr[0]) + below_or,
                            int(es_origin_mr[1]) - width:int(es_origin_mr[1]) + width]

    data_es_US = data_es_US[int(es_origin_us[0]) - above_or:int(es_origin_us[0]) + below_or,
                            int(es_origin_us[1]) - width:int(es_origin_us[1]) + width]

    label_ed_MR -= [int(ed_origin_mr[0]) - above_or,
                    int(ed_origin_mr[1]) - width]
    label_ed_US -= [int(ed_origin_us[0]) - above_or,
                    int(ed_origin_us[1]) - width]
    label_es_MR -= [int(es_origin_mr[0]) - above_or,
                    int(es_origin_mr[1]) - width]
    label_es_US -= [int(es_origin_us[0]) - above_or,
                    int(es_origin_us[1]) - width]

    # Rotate in alax cases, following the annotation rules
    if fus['view'].value == b'alax':

        data_ed_US = np.flip(data_ed_US, axis=1)
        data_es_US = np.flip(data_es_US, axis=1)

        label_ed_US = np.flip(label_ed_US, 0)
        label_es_US = np.flip(label_es_US, 0)

    # Rotate if MR and US is morrored acording to annotation marks
    if fus['view'].value == b'a4ch':

        xgy, xry = label_ed_MR[0, 1], label_ed_MR[2, 1]
        ory, ogy = label_ed_US[2, 1], label_ed_US[0, 1]

        if (xgy < xry and ory < ogy) or (xgy > xry and ory > ogy):

            data_ed_US = np.flip(data_ed_US, axis=1)
            data_es_US = np.flip(data_es_US, axis=1)

            label_ed_US = np.flip(label_ed_US, 0)
            label_es_US = np.flip(label_es_US, 0)

    plot_alignment(data_ed_MR, data_ed_US, label_ed_MR,
                   label_ed_US, fus['view'].value)

    # plot_alignment_both(data_ed_MR, data_ed_US, label_ed_MR,
    #                     label_ed_US, data_es_MR, data_es_US, label_es_MR,
    #                     label_es_US, fus['view'].value)

    # if fus['view'].value == b'a4ch': #'''fus['view'].value == b'alax' or '''

    # print('Shapes after cropping (ed_MR, es_MR, ed_US, es_US) : {} {} {} {}'.format(data_ed_MR.shape, data_es_MR.shape,data_ed_US.shape,data_es_US.shape))
    # plot(data_ed_MR, data_ed_US)
    # plot_alignment(data_ed_MR, data_ed_US, label_ed_MR, label_ed_US, fus['view'].value)

    # fig = plt.figure()
    # plt.subplot(121)
    # plt.imshow(data_ed_MR, cmap='gray')
    # # plt.scatter(label_ed_MR[:,1],label_ed_MR[:,0])
    # # plt.scatter(ed_origin_mr[1],ed_origin_mr[0])
    # plt.title('MR view: {}'.format(fmr['view'].value))
    # plt.subplot(122)
    # plt.imshow(data_ed_US, cmap='gray')
    # # plt.scatter(label_ed_US[:,1],label_ed_US[:,0])
    # plt.title('US view: {}'.format(fus['view'].value))
    # # plt.subplot_tool()
    # plt.show()

    return data_ed_MR, data_ed_US, label_ed_MR, label_ed_US, data_es_MR, data_es_US, label_es_MR, label_es_US, fus['view'].value

    # return data_ed_MR, data_ed_US, data_es_MR, data_es_US


def plot_alignment(mr, us, mark_mr, mark_us, view):
    us -= 1
    us = abs(us)

    mr_colormap = 'gray'
    us_colormap = 'gray'
    color_sequence = ['green', 'blue', 'red']

    fig = plt.figure()
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())

    # plt.subplot(131)
    # plt.imshow(mr, cmap=mr_colormap, alpha=1)
    # plt.scatter(mark_mr[:, 1], mark_mr[:, 0], color=color_sequence, marker='o')
    # plt.imshow(us, cmap=us_colormap, alpha=0.18)
    # plt.scatter(mark_us[:, 1], mark_us[:, 0], color=color_sequence, marker='x')
    # plt.title('Overlap')

    plt.subplot(121)
    plt.imshow(mr, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_mr[:, 1], mark_mr[:, 0], color=color_sequence, marker='o')
    plt.title('MR')

    plt.subplot(122)
    plt.imshow(us, cmap=us_colormap, alpha=1)
    plt.scatter(mark_us[:, 1], mark_us[:, 0], color=color_sequence, marker='x')
    plt.title('US')
    plt.suptitle(b'Alignment of MR and US image pairs - ' + view)
    plt.show()


def plot_alignment_both(mr_ed, us_ed, mark_mr_ed, mark_us_ed, mr_es, us_es, mark_mr_es, mark_us_es,  view, fname):

    us_ed -= 1
    us_ed = abs(us_ed)

    mr_colormap = 'gray'
    us_colormap = 'hot'
    color_sequence = ['green', 'blue', 'red']

    fig = plt.figure(figsize=(30, 20))
    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())

    plt.subplot(231)
    plt.imshow(mr_ed, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_mr_ed[:, 1], mark_mr_ed[:, 0],
                color=color_sequence, marker='o')
    plt.imshow(us_ed, cmap=us_colormap, alpha=0.18)
    plt.scatter(mark_us_ed[:, 1], mark_us_ed[:, 0],
                color=color_sequence, marker='x')
    plt.title('Overlap')

    plt.subplot(232)
    plt.imshow(mr_ed, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_mr_ed[:, 1], mark_mr_ed[:, 0],
                color=color_sequence, marker='o')
    plt.title('MR')

    plt.subplot(233)
    plt.imshow(us_ed, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_us_ed[:, 1], mark_us_ed[:, 0],
                color=color_sequence, marker='x')
    plt.title('US')

    plt.subplot(234)
    plt.imshow(mr_es, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_mr_es[:, 1], mark_mr_es[:, 0],
                color=color_sequence, marker='o')
    plt.imshow(us_es, cmap=us_colormap, alpha=0.18)
    plt.scatter(mark_us_es[:, 1], mark_us_es[:, 0],
                color=color_sequence, marker='x')

    plt.subplot(235)
    plt.imshow(mr_es, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_mr_es[:, 1], mark_mr_es[:, 0],
                color=color_sequence, marker='o')

    plt.subplot(236)
    plt.imshow(us_es, cmap=mr_colormap, alpha=1)
    plt.scatter(mark_us_es[:, 1], mark_us_es[:, 0],
                color=color_sequence, marker='x')
    plt.suptitle(b'Alignment of MR and US image pairs - ' + view)

    plt.savefig(os.path.join('..', '..', 'data', 'alignment_plot', fname))


def plot_4(mr_ed, us_ed, mr_es, us_es):
    fig = plt.figure()
    plt.subplot(221), plt.imshow(mr_ed, cmap='gray')
    plt.title('mr_ed')
    plt.subplot(222), plt.imshow(us_ed, cmap='gray')
    plt.title('us_ed')
    plt.subplot(223), plt.imshow(mr_es, cmap='gray')
    plt.title('mr_es')
    plt.subplot(224), plt.imshow(us_es, cmap='gray')
    plt.title('us_es')
    plt.show()


def plot_2(data_1, data_2):
    fig = plt.figure()
    plt.subplot(121), plt.imshow(data_1, cmap='gray')
    plt.subplot(122), plt.imshow(data_2, cmap='gray')
    plt.show()


def plot_pair(fmr, fus):

    # Get numpy image pixel data
    data_ed_MR, data_es_MR = get_big_MR(fmr)
    data_ed_US, data_es_US = fus['data_ed'].value, fus['data_es'].value

    fig = plt.figure()
    plt.subplot(121)
    plt.imshow(data_ed_MR, cmap='gray')

    plt.title('MR view: {}'.format(fmr['view'].value))
    plt.subplot(122)
    plt.imshow(data_ed_US, cmap='gray')

    plt.title('US view: {}'.format(fus['view'].value))
    # plt.subplot_tool()
    plt.show()

    fig = plt.figure()
    plt.subplot(121)
    plt.imshow(data_es_MR, cmap='gray')

    plt.title('MR view: {}'.format(fmr['view'].value))
    plt.subplot(122)
    plt.imshow(data_es_US, cmap='gray')

    plt.title('US view: {}'.format(fus['view'].value))
    # plt.subplot_tool()
    plt.show()


def find_h_axis_rotation(f):

    label_ed = f['label_ed'].value
    label_es = f['label_es'].value

    ed_origin = label_ed[1]
    es_origin = label_es[1]

    ed1 = label_ed[0]
    ed2 = label_ed[2]

    es1 = label_es[0]
    es2 = label_es[2]

    ed_m = (ed1 + ed2) / 2
    es_m = (es1 + es2) / 2

    ed_vec = ed_m - ed_origin
    es_vec = es_m - es_origin

    ed_angle = angle(ed_vec, [1, 0])
    es_angle = angle(es_vec, [1, 0])

    return rad2deg(ed_angle), rad2deg(es_angle), ed_origin, es_origin


def rad2deg(angle):
    return math.degrees(angle)


def deg2rad(angle):
    return math.radians(angle)


def dotproduct(v1, v2):
    return sum((a*b) for a, b in zip(v1, v2))


def length(v):
    return math.sqrt(dotproduct(v, v))


def angle(v1, v2):
    x1, y1 = v1[0], v1[1]
    x2, y2 = v2[0], v2[1]

    dot = x1 * x2 + y1 * y2
    det = x1 * y2 - y1 * x2
    angle = math.atan2(det, dot)
    return angle


def printPos(fmr_list, fus_list, MR_patient_path, US_patient_path):

    fmr_pos = []
    fus_pos = []

    for i, name in enumerate(fmr_list):
        fMR = h5py.File(os.path.join(MR_patient_path, name), 'r')
        fmr_pos.append(fMR['view'].value)
    for i, name in enumerate(fus_list):
        fUS = h5py.File(os.path.join(US_patient_path, name), 'r')
        fus_pos.append(fUS['view'].value)

    return fmr_pos, fus_pos


def printUSpos(fus_list, US_patient_path):
    fus_pos = []
    for i, name in enumerate(fus_list):
        fUS = h5py.File(os.path.join(US_patient_path, name), 'r')
        fus_pos.append(fUS['view'].value)

    return fus_pos


def MRinUS_list(pos, list):
    indices = []
    pos_US = MR2US_posdict[pos]
    for i, pos_name in enumerate(list):
        if pos_US == pos_name:
            indices.append(i)

    return indices


MR2US_posdict = {b'2ch': b'a2ch',
                 b'4ch': b'a4ch',
                 b'lax': b'alax',
                 b'5ch': b'a5ch',
                 b'sax': b'alax'}


# Creating patient folders for image matching
png_basepath_MR = os.path.join('..', '..', 'data', 'datapairs_png', 'MR')
png_basepath_US = os.path.join('..', '..', 'data', 'datapairs_png', 'US')


# if not os.path.isdir(png_basepath_MR):
#     os.mkdir(png_basepath_MR)
# if not os.path.isdir(png_basepath_US):
#     os.mkdir(png_basepath_US)


MRpath = os.path.join('..', '..', '..', 'LandmarkCardiotoxityMR_distilled')
USpath = os.path.join('..', '..', '..', 'LandmarkCardiotoxityUS_distilled')

patient_list_MR = os.listdir(MRpath)
patient_list_US = os.listdir(USpath)

# Take only the patients present in both MR and US folder list
patient_list = list(set(patient_list_MR) | set(patient_list_US))
patient_list = natsorted(patient_list)

save_base_path_US_extraction = os.path.join(
    '..', '..', 'rawdata', 'MR-US_US_data', 'rawdata')

print('A total of {} patients found'.format(len(patient_list)))

if __name__ == '__main__':

    # Script for extracting all US data on this dataset
    patients = 0
    for i, PID in enumerate(patient_list):
        US_patient_path = os.path.join(USpath, PID)
        try:
            USstreams = os.listdir(US_patient_path)
            fus_pos_list = printUSpos(USstreams, US_patient_path)
            US_index = 0
            print('Saving data found on patient: {}'.format(PID))

            for i, fus_pos in enumerate(fus_pos_list):
                US_index = i

                fUS = h5py.File(os.path.join(
                    US_patient_path, USstreams[US_index]), 'r')

                data_ed_US, data_es_US = fUS['data_ed'].value, fUS['data_es'].value

                im_ed_savename = str(PID) + '_' + str(US_index) + '_ed.png'
                im_es_savename = str(PID) + '_' + str(US_index) + '_es.png'

                scipy.misc.imsave(os.path.join(
                    save_base_path_US_extraction, im_ed_savename), data_ed_US)

                scipy.misc.imsave(os.path.join(
                    save_base_path_US_extraction, im_es_savename), data_es_US)
        except OSError:
            print('No data found on patient: {}'.format(PID))
    exit(0)

    for i, PID in enumerate(patient_list):
        MR_patient_path = os.path.join(MRpath, PID)
        US_patient_path = os.path.join(USpath, PID)

        try:
            MRstreams = os.listdir(MR_patient_path)
            USstreams = os.listdir(US_patient_path)

            MR_index = 0
            US_index = 0
            fmr_pos_list, fus_pos_list = printPos(
                MRstreams, USstreams, MR_patient_path, US_patient_path)

            count = 0

            for i, fmr_pos in enumerate(fmr_pos_list):
                fus_indices = MRinUS_list(fmr_pos, fus_pos_list)

                for j, index in enumerate(fus_indices):
                    MR_index = i
                    US_index = index

                    # open the matching streams
                    fMR = h5py.File(os.path.join(
                        MR_patient_path, MRstreams[MR_index]), 'r')
                    fUS = h5py.File(os.path.join(
                        US_patient_path, USstreams[US_index]), 'r')
                    # plot_pair(fMR,fUS)

                    data_ed_US, data_es_US = fUS['data_ed'].value, fUS['data_es'].value

                    # # Sax positions has no landmarks, so skipping those
                    # if fMR['view'].value == b'sax' or fUS['view'].value == b'sax':
                    #     print('Skipping sax positions')
                    # else:
                    #     # Create name for files
                    #     ed_name = PID + '_' + str(i) + str(j) + '_ed.png'
                    #     es_name = PID + '_' + str(i) + str(j) + '_es.png'

                    #     print('\nImage stream view: {}'.format(fMR['view']))
                    #     print('ED : {}'.format(ed_name))
                    #     print('ES : {}'.format(es_name))
                    #     # Align ed and es image pairs
                    #     # data_ed_MR, data_ed_US, data_es_MR, data_es_US = align_US_and_MR(
                    #     # fMR, fUS)
                    #     if i > 1:
                    #         data_ed_MR, data_ed_US, label_ed_MR, label_ed_US, data_es_MR, data_es_US, label_es_MR, label_es_US, fus_view = align_US_and_MR(
                    #             fMR, fUS)

                    #     # plot_alignment_both(data_ed_MR, data_ed_US, label_ed_MR,
                    #     #                     label_ed_US, data_es_MR, data_es_US, label_es_MR,
                    #     #                     label_es_US, fus_view, PID + '_' + str(i) + str(j) + '.png')

                    #     # Store images, MR and US has identical name
                    #     # scipy.misc.imsave(os.path.join(
                    #     #     png_basepath_MR, ed_name), data_ed_MR)
                    #     # scipy.misc.imsave(os.path.join(
                    #     #     png_basepath_US, ed_name), data_ed_US)

                    #     # scipy.misc.imsave(os.path.join(
                    #     #     png_basepath_MR, es_name), data_es_MR)
                    #     # scipy.misc.imsave(os.path.join(
                    #     #     png_basepath_US, es_name), data_es_US)

                    #     count += 1
            print('{} pairs from {}'.format(count, PID))
            patients += 1

        except OSError:
            print('Both MR and US data not found on patient: {}'.format(PID))

    print('{} patients processed'.format(patients))
