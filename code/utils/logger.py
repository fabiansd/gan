import matplotlib.pyplot as plt
import numpy as np
import os
import h5py
import time

from utils.helpfunctions import denormalize


def inverse_normalization(X):
    return X * 255.0


def plot_generated_batch(X_full, X_sketch, generator_model, epoch_num, dataset_name, batch_num, save_folder):

    training_save_folder = os.path.join(save_folder)

    if not os.path.isdir(training_save_folder):
        os.mkdir(training_save_folder)

    # Generate images
    ts = time.time()
    # print('Starting timing at {}'.format(ts))
    X_gen = generator_model.predict(X_sketch)

    ts_delta = time.time()
    # print('prediction timing with batch size {} is {}'.format(
    # X_full.shape[0], ts_delta-ts))
    timing = ts_delta-ts

    X_sketch = denormalize(X_sketch)
    X_full = denormalize(X_full)
    X_gen = denormalize(X_gen)

    # limit to 4 images as output
    Xs = X_sketch[:4]
    Xg = X_gen[:4]
    Xr = X_full[:4]

    # put |decoded, generated, original| images next to each other

    X = np.concatenate((Xs, Xg, Xr), axis=2)

    # make one giant block of images (merge batches)
    X = np.concatenate(X, axis=0)

    # save the giant n x 3 images
    plt.imsave(os.path.join(training_save_folder, '{}_epoch_{}_batch_{}.png'.format(
        dataset_name, epoch_num, batch_num)), X[:, :, 0], cmap='Greys_r')

    return timing


def initialize_save_folders(direction, st, save_folder_name):

    if not os.path.isdir(os.path.join('results', save_folder_name)):
        os.mkdir(os.path.join('results', save_folder_name))

    save_folder = os.path.join(
        'results', save_folder_name, 'results_{}_{}'.format(direction, st))

    # create dir for saving results
    if not os.path.isdir(os.path.join(save_folder_name, 'results_{}_{}'.format(direction, st))):
        os.mkdir(os.path.join('results', save_folder_name,
                              'results_{}_{}'.format(direction, st)))

    log_dataset = h5py.File(os.path.join(
        'results', save_folder_name, 'results_{}_{}'.format(direction, st), 'loss_history.h5'), 'w')

    return save_folder, log_dataset

# Writes a short summary of the network


def write_metadata(save_folder, direction, n_epochs, batch_size, image_size,
                   sub_image_size, disc_optimizer, discriminator_gaussian_noise, label_smoothing, note):

    f = open(os.path.join(save_folder, 'metafile.txt'), 'w')

    f.write('Direction: {}\n'.format(direction))
    f.write('Epochs trained: {}\n'.format(n_epochs))
    f.write('Batch size: {}\n'.format(batch_size))
    f.write('Label smoothing: {}\n'.format(label_smoothing))
    f.write('image_size: {}\n'.format(image_size))
    f.write('sub_image_size: {}\n'.format(sub_image_size))
    f.write('discriminator optimizer: {}\n'.format(disc_optimizer))
    f.write('Gaussian noise discriminator: {}\n'.format(
        discriminator_gaussian_noise))
    f.write('{}\n'.format(note))
    f.close()
