import SimpleITK as sitk
from PIL import Image
import sys
import os
import numpy as np
import glob
import argparse
import matplotlib.pyplot as plt

from medpy.io import load
from mhd_utils import *


parser = argparse.ArgumentParser(
    description='script for extracting mhd images, reads only from folder: mhd data in OneDrive. Dataset is saved in rawdata folder')
parser.add_argument('--folder', required=True,
                    help='Select folder to extract images from')
parser.add_argument('--name', required=True,
                    help='Select name for resulting dataset')
parser.add_argument('--vis', required=False, type=bool, default=False,
                    help='Write True or False whether you want to visualize')
args = parser.parse_args()
folder = args.folder
name = args.name
vis = args.vis

folder_path = os.path.join('..', '..', '..', 'mhd_data', folder)

try:
    image_list = glob.glob(os.path.join(folder_path, '*.mhd'))
except OSError:
    print('no data found on: {}'.format(folder_path))
    exit(1)

if len(image_list) == 0:
    print('No images in destination: {}'.format(folder_path))
    exit(1)

save_path = os.path.join('..', '..', 'rawdata', name)

if not os.path.isdir(save_path):
    os.mkdir(save_path)


save_path = os.path.join(save_path, 'rawdata')
if not os.path.isdir(save_path):
    os.mkdir(save_path)
first = True
for i, filepath in enumerate(image_list):

    # image_data = load_raw_data_with_mhd(filepath)

    # print(image_data)
    # print(image_data.shape)
    # exit(0)
    itkimage = sitk.ReadImage(filepath)

    # Convert the image to a  numpy array first and then shuffle the dimensions to get axis in the order z,y,x
    im_np = sitk.GetArrayFromImage(itkimage)
    PIL_im = Image.fromarray(im_np)

    save_file_path = os.path.join(save_path, str(i) + '_' + name + '.png')
    PIL_im.save(save_file_path)

    # org_size = PIL_im.size
    # print(org_size)

    # spacing_reference = 1

    # spacing = np.array(list((itkimage.GetSpacing())))

    # new_size = ((1/spacing[0])*org_size[0], (1/spacing[0])*org_size[0])

    # print(spacing)

    # upscaling = np.multiply((spacing_reference/spacing),
    #                         im_np.shape).astype('uint8')

    # PIL_im_respaced = PIL_im.resize(
    #     (upscaling[0], upscaling[1]), Image.BICUBIC)

    # Read the origin of the ct_scan, will be used to convert the coordinates from world to voxel and vice versa.
    # origin = np.array(list(reversed(itkimage.GetOrigin())))

    # if first:

    #     fig = plt.figure()
    #     plt.imshow(PIL_im, cmap='gray')
    #     plt.show()

    #     first = False
