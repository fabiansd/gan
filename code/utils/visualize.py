import numpy as np
# from natsort import natsorted
from PIL import Image
import matplotlib.pyplot as plt
import os
import h5py
from PIL import Image, ImageFilter
import matplotlib.pyplot as plt


def show_image_pairs(orglist, targlist, subfig=(2, 2)):

    fig, axs = plt.subplots(subfig[0], subfig[1], figsize=(
        15, 10), facecolor='w', edgecolor='k')

    axs = axs.ravel()

    for i in range(subfig[0]*subfig[1]):

        image = np.concatenate((orglist[i], targlist[i]), axis=2)

        axs[i].imshow(image[0, :, :], cmap='gray')

    plt.show()


def show_single_pair(org, targ):

    fig, axs = plt.subplots(1, 1, figsize=(
        15, 10), facecolor='w', edgecolor='k')

    image = np.concatenate((org, targ), axis=2)

    axs.imshow(image[0, :, :], cmap='gray')

    plt.show()


def show_single_image(img, title=''):

    fig, axs = plt.subplots(1, 1, figsize=(
        15, 10), facecolor='w', edgecolor='k')
    if len(img.shape) == 2:
        axs.imshow(img, cmap='gray')
    elif len(img.shape) == 3 and img.shape[0] <= 3:
        axs.imshow(img[0, :, :], cmap='gray')
    elif len(img.shape) == 3 and img.shape[2] <= 3:
        axs.imshow(img[:, :, 0], cmap='gray')
    else:
        print('Invalid image format in "show_single_image" function')
        exit(1)
    plt.title(title)
    plt.show()


def visualize_loss_history(savepath, start=None, end=None, display=False):

    history = h5py.File(os.path.join(savepath, 'loss_history.h5'), 'r')

    im_savapath = os.path.join(savepath, 'training_plots')
    if not os.path.isdir(im_savapath):
        os.mkdir(im_savapath)

    metrics = list(history.keys())

    dis_logloss = (history[metrics[0]].value)
    gen_l1 = (history[metrics[1]].value)
    gen_logloss = (history[metrics[2]].value)
    gen_loss = (history[metrics[3]].value)

    ax = plt.figure(figsize=(20, 15))
    # plt.figure()
    plt.plot(history[metrics[0]].value)
    plt.plot(history[metrics[1]].value)
    plt.plot(history[metrics[2]].value)
    # plt.plot(history[metrics[3]].value)
    plt.title('Gan training loss')
    plt.ylabel('Loss')
    plt.xlabel('Iterations')
    plt.legend(metrics[0:-1], loc='upper right')
    if (start != None) and (end != None):
        plt.xlim(start, end)

    plt.savefig(os.path.join(
        im_savapath, 'all_loss_epoch_{}_to_{}.png'.format(start, end)))

    ax = plt.figure(figsize=(20, 15))
    # plt.figure()
    plt.plot(history[metrics[0]].value)
    # plt.plot(history[metrics[1]].value)
    # plt.plot(history[metrics[2]].value)
    # plt.plot(history[metrics[3]].value)
    plt.title('Gan training loss')
    plt.ylabel('Loss')
    plt.xlabel('Iterations')
    plt.legend(metrics[0:1], loc='upper right')
    if (start != None) and (end != None):
        plt.xlim(start, end)

    plt.savefig(os.path.join(
        im_savapath, 'disc_training_epoch_{}_to_{}.png'.format(start, end)))

    ax = plt.figure(figsize=(20, 15))
    # plt.figure()
    # plt.plot(history[metrics[0]].value)
    plt.plot(history[metrics[1]].value)
    # plt.plot(history[metrics[2]].value)
    # plt.plot(history[metrics[3]].value)
    plt.title('Gan training loss')
    plt.ylabel('Loss')
    plt.xlabel('Iterations')
    plt.legend(metrics[1:2], loc='upper right')
    if (start != None) and (end != None):
        plt.xlim(start, end)

    plt.savefig(os.path.join(
        im_savapath, 'L1_training_epoch_{}_to_{}.png'.format(start, end)))

    ax = plt.figure(figsize=(20, 15))
    # plt.figure()
    # plt.plot(history[metrics[0]].value)
    # plt.plot(history[metrics[1]].value)
    plt.plot(history[metrics[2]].value)
    # plt.plot(history[metrics[3]].value)
    plt.title('Gan training loss')
    plt.ylabel('Loss')
    plt.xlabel('Iterations')
    plt.legend(metrics[2:3], loc='upper right')
    if (start != None) and (end != None):
        plt.xlim(start, end)

    plt.savefig(os.path.join(
        im_savapath, 'gen_logloss_training_epoch_{}_to_{}.png'.format(start, end)))

    if display:
        plt.show()


def visualize_dataset():
    data_path = os.path.join('..', '..', 'data', 'datapairs_png')

    mr_path = os.path.join(data_path, 'MR')
    us_path = os.path.join(data_path, 'US')

    mr_images = natsorted(os.listdir(mr_path))
    us_images = natsorted(os.listdir(us_path))

    chunk = 16
    rows = 4
    cols = 4

    tot_im = len(mr_images)

    for i_list in range(0, tot_im, chunk):

        fig = plt.figure(figsize=(12, 15))
        # plt.title('Images {} to {}'.format(i_list, i_list+chunk))
        image_num = 1

        for idx in range(i_list, i_list+chunk):
            im_mr = np.array(Image.open(os.path.join(mr_path, mr_images[idx])))
            im_us = np.array(Image.open(os.path.join(us_path, us_images[idx])))

            im_tot = np.concatenate((im_mr, im_us), axis=1)

            fig.add_subplot(rows, cols, image_num)
            plt.imshow(im_tot, cmap='gray')

            image_num += 1
        plt.show()

# Fix such that it runs automatically


def save_vis(savepath):

    #  automatically selecting save folder
    save_folder_name_list = ['network_save',
                             'speckle_reduction', 'mr_resoncstruction']

    savepath = os.path.join('..', 'results', save_folder_name_list[save_folder_index],
                            network,)

    # Call visualizer with boundaries
    visualize_loss_history(savepath, 0, 200, display=False)
    visualize_loss_history(savepath, 1800, 2000)
    visualize_loss_history(savepath, 0, 1500)


def gaussian_smoothing(path, rad):
    im = Image.open(path)

    im_filtered = im.filter(ImageFilter.GaussianBlur(radius=rad))

    savepath = (os.path.split(path))[0]
    name = (os.path.split(path))[-1][0:-4]

    savepath = (os.path.join(savepath, name + '_gauss' + str(rad) + '.png'))

    im_filtered.save(savepath)
    plt.imshow(im_filtered, cmap='gray')
    plt.show()


if __name__ == '__main__':

    import sys

    gaussian_path = os.path.join('..', '..', 'images for report',
                                 'Abstract', 'sample2', 'sample2_org.png')
    # gaussian_smoothing(gaussian_path, 4)
    # exit(0)
    try:
        network = sys.argv[1]
        save_folder_index = sys.argv[2]
    except IndexError:
        print('no network given')
        network = 'results_speckle2specklefree_M05-D23_h10-m24-s42'
        save_folder_index = 1

    #  automatically selecting save folder
    save_folder_name_list = ['network_save',
                             'speckle_reduction', 'mr_resoncstruction']

    savepath = os.path.join('..', 'results', save_folder_name_list[save_folder_index],
                            network,)
    # Call visualizer with boundaries
    visualize_loss_history(savepath, 0, 200, display=False)
    visualize_loss_history(savepath, 1800, 2000)
    visualize_loss_history(savepath, 0, 1500)
