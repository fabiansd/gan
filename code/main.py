import os
import argparse
import sys
import time
import datetime
from models import discriminator, generator, DCGAN
from keras.optimizers import Adam, SGD, RMSprop
from keras.utils import generic_utils as keras_generic_utils

from utils import logger, visualize, arrange_data, patch_utils, image_generator
from models.model_util import WGAN_loss

import h5py

import numpy as np


if __name__ == '__main__':

    # REMEMBER THAT THE MODEL SAVES EVERY 100, 120 140 epochs

    # Control variables for what experiment to run
    select_dataset = 3
    # ------HYPERPARAMETERS---------
    n_epochs = 170
    batch_size = 4
    validation_batch_size = 2
    test_batch_size = 1
    image_size = (512, 512)
    sub_image_size = (64, 64)
    disc_optimizer = 'SGD'
    generator_gaussian_noise = 0
    discriminator_gaussian_noise = 0
    note = 'DCGAN, training with loss weight [1 1]'
    # None for no invertion of MR images
    label_smoothing = True
    invert_direction = False
    color = False
    # -------------------------------
    channel_format = 'channels_last'

    # Select data for usage
    direction_list = ['MR2US', 'US2MR',
                      'US2MR_distilled', 'speckle2specklefree', 'MR2MR']
    direction = direction_list[select_dataset]
    dataname_list = ['MR2US_original_mrus_data', 'US2MR_original_mrus_data', 'US2MR_distilled',
                     'speckle_reduction_data', 'MR_reconstruction_data']
    dataname = dataname_list[select_dataset]

    # Create datapth
    datapath = os.path.join('..', 'data', dataname)

    #  automatically selecting save folder
    save_folder_name_dict = {'MR2US_original_mrus_data': 'network_save', 'US2MR_original_mrus_data': 'network_save', 'US2MR_distilled': 'network_save',
                             'speckle_reduction_data': 'speckle_reduction', 'MR_reconstruction_data': 'mr_resoncstruction'}
    save_folder_name = save_folder_name_dict[dataname]

    # Selecting GPU to train on

    parser = argparse.ArgumentParser(
        description='DCGAN for learning a given image translation')
    parser.add_argument('--gpu', required=True,
                        help='Select which GPU to train on')
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    print('Training on GPU {}'.format(os.environ['CUDA_VISIBLE_DEVICES']))

    # Get timestamp
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('M%m-D%d_h%H-m%M-s%S')

    # Initialize folders for saving
    save_folder, log_dataset = logger.initialize_save_folders(
        direction, st, save_folder_name)

    # Temporary lists for storing loss values
    dis_logloss_list = []
    gen_total_list = []
    gen_L1_list = []
    gen_logloss_list = []

    # Get the shape of the data
    dict_image_channels = {False: 1, True: 3}
    input_shape = (image_size[0], image_size[1], dict_image_channels[color])
    output_shape = (image_size[0], image_size[1], dict_image_channels[color])

    # Extracting number of patches and the dimension of those patches for the discriminator
    nb_patch_patches, patch_gan_dim = patch_utils.num_patches(channel_format=channel_format,
                                                              output_img_dim=output_shape, sub_patch_dim=sub_image_size)

    # Store hyperparameters as txt file
    logger.write_metadata(save_folder, direction, n_epochs, batch_size, image_size,
                          sub_image_size, disc_optimizer, discriminator_gaussian_noise, label_smoothing, note)

    # Get size of the data buckets, training, validation and testing
    org_samples_training, targ_samples_training = image_generator.get_n_samples(
        datapath, 'training', batch_size)
    org_samples_validating, targ_samples_validating = image_generator.get_n_samples(
        datapath, 'validating', validation_batch_size)
    org_samples_testing, targ_samples_testing = image_generator.get_n_samples(
        datapath, 'testing', test_batch_size)

    print('Data size:')
    print('Training   (org, targ) : ({}, {})'.format(
        org_samples_training, targ_samples_training))
    print('Validation (org, targ) : ({}, {})'.format(
        org_samples_validating, targ_samples_validating))
    print('Testing    (org, targ) : ({}, {})'.format(
        org_samples_testing, targ_samples_testing))
    print()

    #############################
    #       Creating model      #
    #############################

    # Optimizers
    opt_dcgan = Adam(lr=2E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    opt_generator = Adam(lr=2E-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    opt_discriminator = SGD(lr=1E-4, momentum=0.9)
    opt_discriminator = Adam(lr=5E-5, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

    # Initialize generator
    generator_nn = generator.slim_UNETGenerator(
        input_img_dim=input_shape, num_output_channels=dict_image_channels[color])
    generator_nn.summary()

    # initialize discriminator
    discriminator_nn = discriminator.PatchGanDiscriminator(output_img_dim=output_shape,
                                                           patch_dim=patch_gan_dim, nb_patches=nb_patch_patches, gaussian_std=discriminator_gaussian_noise)
    discriminator_nn.trainable = False
    discriminator_nn.summary()

    generator_nn.compile(loss='mae', optimizer=opt_generator)

    # Initialize GAN
    h, w = input_shape[:2]
    ph, pw = sub_image_size

    # chop the generated image into patches, lists with coordinates of patches

    dc_gan_nn = DCGAN.DCGAN(generator_model=generator_nn, discriminator_model=discriminator_nn,
                            input_img_dim=input_shape, patch_dim=sub_image_size)

    dcgan_loss = ['mae', 'binary_crossentropy']
    dcgan_loss_weights = [1, 1]
    # dcgan_loss_weights = [1, 0]
    dc_gan_nn.compile(loss=dcgan_loss, loss_weights=dcgan_loss_weights,
                      optimizer=opt_dcgan)
    dc_gan_nn.summary()

    discriminator_nn.trainable = True
    discriminator_nn.compile(loss=['binary_crossentropy'],
                             optimizer=opt_discriminator)

    for epoch in range(n_epochs):
        # TRAINING PHASE
        print('Epoch {}'.format(epoch))
        start = time.time()
        batch_counter = 1
        progbar = keras_generic_utils.Progbar(org_samples_training)

        # Reinitializing data every epoch
        train_gen = image_generator.image_generator(
            datapath, 'training', image_size, batch_size, invert_direction=invert_direction, random_seed=epoch, color=color)

        valid_gen = image_generator.image_generator(
            datapath, 'validating', image_size, validation_batch_size, invert_direction=invert_direction, random_seed=epoch, color=color)

        for i in range(0, org_samples_training, batch_size):

            #####################################
            #       Discriminator training      #
            #####################################

            input_train, target_train = next(train_gen)

            # visualize.show_single_pair(
            #     input_train[0], target_train[0])

            X_discriminator, y_discriminator = patch_utils.get_disc_batch(target_train,
                                                                          input_train,
                                                                          generator_nn,
                                                                          batch_counter,
                                                                          patch_dim=sub_image_size,
                                                                          label_smoothing=label_smoothing)

            # visualize.show_single_pair(
            #     X_discriminator[0][0], X_discriminator[0][0])

            # Training discriminator
            disc_loss = discriminator_nn.train_on_batch(
                X_discriminator, y_discriminator)

            #####################################
            #       Gan training                #
            #####################################

            # Get two batches in order to train G an equal amount of D
            # input_train_1, target_train_1 = next(train_gen)
            # input_train_2, target_train_2 = next(train_gen)
            # input_train = np.concatenate((input_train_1, input_train_2))
            # target_train = np.concatenate((target_train_1, target_train_2))

            X_gen_input, X_gen_target = next(
                patch_utils.gen_batch(input_train, target_train, batch_size))

            # visualize.show_single_pair(
            #     X_gen_input[0], X_gen_target[0])

            # Fake images labelled as real in order to train the G to fool D
            y_gen = np.zeros((X_gen_input.shape[0], 2), dtype=np.uint8)

            if label_smoothing:
                y_gen = np.zeros((X_gen_input.shape[0], 2), dtype='f')
                y_gen[:, 1] = np.random.uniform(
                    low=0.9, high=1, size=y_gen.shape[0])
            else:
                y_gen[:, 1] = 1

            # Freeze the discriminator
            discriminator_nn.trainable = False

            # Trainining GAN
            gen_loss = dc_gan_nn.train_on_batch(
                X_gen_input, [X_gen_target, y_gen])

            # Unfreeze the discriminator
            discriminator_nn.trainable = True

            batch_counter += 1

            # print losses
            D_log_loss = disc_loss
            gen_total_loss = gen_loss[0].tolist()
            gen_total_loss = min(gen_total_loss, 1000000)
            gen_mae = gen_loss[1].tolist()
            gen_mae = min(gen_mae, 1000000)
            gen_log_loss = gen_loss[2].tolist()
            gen_log_loss = min(gen_log_loss, 1000000)

            progbar.add(batch_size, values=[("Dis logloss", D_log_loss),
                                            ("Gen total", gen_total_loss),
                                            ("Gen L1 (mae)", gen_mae),
                                            ("Gen logloss", gen_log_loss)])
            # Log loss values for history file
            dis_logloss_list.append(D_log_loss)
            gen_total_list.append(gen_total_loss)
            gen_L1_list.append(gen_mae)
            gen_logloss_list.append(gen_log_loss)

            input_val, target_val = next(valid_gen)
            # -------------:--------------
            # Save images for visualization for 6 batches, does this every
            # epoch the first 20 epochs, then every 10th epoch
            if (int(i / batch_size) % int(100 / batch_size) == 0):
                if (epoch < 10) or (epoch % 15 == 0) or ((epoch > 100) and (epoch < 130)):
                    # print images for training data progress
                    logger.plot_generated_batch(
                        target_train, input_train, generator_nn, epoch, 'tng', i, os.path.join(save_folder, 'training_images'))

                    # print images for validation data

                    X_batch_val_target, X_batch_val_input = next(
                        patch_utils.gen_batch(target_val, input_val, validation_batch_size))

                    logger.plot_generated_batch(
                        X_batch_val_target, X_batch_val_input, generator_nn, epoch, 'val', i, os.path.join(save_folder, 'training_images'))

        # -----------------------
        # log epoch
        print("")
        print('Epoch %s/%s, Time: %s' %
              (epoch + 1, n_epochs, time.time() - start))

        # ------------------------------
        # save model every nth epoch
        # ((epoch % 100 == 0) and (epoch > 110)) or epoch + 1 == n_epochs:
        if epoch == 100 or epoch == 130:
            model_save_path = os.path.join(
                save_folder, 'gen_model_epoch_{}_{}.h5'.format(epoch, direction))

            generator_nn.save(model_save_path, overwrite=True)

    # Store loss history in h5py file
    log_dataset.create_dataset('Dis logloss', data=dis_logloss_list)
    log_dataset.create_dataset('Gen logloss', data=gen_logloss_list)
    log_dataset.create_dataset('Gen L1 (mae)', data=gen_L1_list)
    log_dataset.create_dataset('Gen total', data=gen_total_list)

    # Final test of gan network
    test_gen = image_generator.image_generator(
        datapath, 'testing', image_size, test_batch_size, invert_direction=invert_direction, color=color)

    # Save final model of the network
    model_save_path = os.path.join(
        save_folder, 'gen_model_epoch_{}_{}.h5'.format(n_epochs, direction))

    generator_nn.save(model_save_path, overwrite=True)

    for index in range(0, org_samples_testing, test_batch_size):

        input_test, target_test = next(test_gen)

        # print images for test set
        X_batch_test_target, X_batch_test_input = next(
            patch_utils.gen_batch(target_test, input_test, test_batch_size))

        logger.plot_generated_batch(
            X_batch_test_target, X_batch_test_input, generator_nn, n_epochs, 'test', index, os.path.join(save_folder, 'training_images'))

    # Save loss history plots
    # visualize.visualize_loss_history(save_folder, 0, 200, display=False)
    # visualize.visualize_loss_history(save_folder, 1000, 1200, display=False)
    # visualize.visualize_loss_history(save_folder, 0, 2000, display=False)
