Direction: US2MR
Epochs trained: 300
Batch size: 1
image_size: (256, 256)
sub_image_size: (64, 64)
label_smoothing: True
label_flipping: False
invert_direction: US2MR
