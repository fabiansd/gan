import os
import parser
from PIL import Image
import sys
import matplotlib.pyplot as plt
import numpy as np
import copy

target_path = os.path.join(
    'network_save', 'US2MR_best_results_2', 'test_epoch_300_batch_6.png')

save_path = ('super_image_folder')

if not os.path.isdir(save_path):
    os.mkdir(save_path)

# parser = argparse.ArgumentParser(
#     description='Script for making super images')
# parser.add_argument('--image_path', required=False,
#                     help='Define image path')
# args = parser.parse_args()

# Assumes batch test size of 2!!!

image = Image.open(target_path).convert('RGBA')
im = np.array(image)
us = im[int(im.shape[0]/2):, :int(im.shape[1]/3)]
mr = im[int(im.shape[0]/2):, int(im.shape[1]/3):int(im.shape[1]*2/3)]

# GENERATING OVERLAP IMAGES

alpha_us = 0.5
alpha_mr = 0.5
mid_index = int(us.shape[0]/2)
inv_mr = np.invert(mr)
inv_mr[:, :, -1] = 255

us_copy = copy.deepcopy(us)
mr_inv_copy = copy.deepcopy(inv_mr)
# inv_mr = mr

us[:mid_index, :, -1] = us[:mid_index, :, -1] * alpha_us
inv_mr[mid_index:, :, -1] = inv_mr[mid_index:, :, -1] * alpha_mr

# GENERATING MULTIPLICATION IMAGES
# inv_mr = np.invert(mr)
# inv_mr = inv_mr / 255.

# results = np.multiply(us, inv_mr)


color_2 = 'gray'
color_1 = 'hot'

fig = plt.figure()
mng = plt.get_current_fig_manager()
# mng.resize(*mng.window.maxsize())

plt.subplot(131)
plt.imshow(us, cmap=color_1, alpha=.8)
plt.imshow(inv_mr, cmap=color_2, alpha=0.8)
plt.title('Overlap')

plt.subplot(132)
plt.imshow(inv_mr, cmap=color_2, alpha=1)
plt.title('MR')

plt.subplot(133)
plt.imshow(us, cmap=color_2, alpha=1)
plt.title('US')
plt.suptitle('Super image')
plt.show()

# i = input('Save image: write image name *.png, else: q')
# if i.endswith('.png'):
#     plt.savefig(i)
