Direction: speckle2specklefree
Epochs trained: 170
Batch size: 4
Label smoothing: True
image_size: (1024, 512)
sub_image_size: (64, 64)
discriminator optimizer: SGD
Gaussian noise discriminator: 0
DCGAN, 256x256 w BN, estreme slim generator
