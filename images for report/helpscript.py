import matplotlib.pyplot as plt
import os
from PIL import Image, ImageChops
import numpy as np
import h5py


def plot_save_image(path, filename, savename):

    path_open = os.path.join(path, filename)

    im = Image.open(path_open)

    fig = plt.figure(figsize=(20, 15))
    plt.imshow(im, cmap='gray')
    plt.title('US abd MR pair, view: b\'2ch\'')
    plt.savefig(os.path.join(path, savename))


def get_l1(network_path):

    file_path = os.path.join('generator_results_archive',
                             network_path, 'loss_history.h5')

    history = h5py.File(file_path, 'r')

    metrics = list(history.keys())

    dis_logloss = (history[metrics[0]].value)
    gen_l1 = (history[metrics[1]].value)
    gen_logloss = (history[metrics[2]].value)
    gen_loss = (history[metrics[3]].value)

    print(metrics)
    print('min values after 1800 epochs')
    print([dis_logloss[1800:].min(), gen_l1[1800:].min(),
           gen_logloss[1800:].min(), gen_loss[1800:].min()])
    print('avg values after 1800 epochs')
    print([dis_logloss[1800:].mean(), gen_l1[1800:].mean(),
           gen_logloss[1800:].mean(), gen_loss[1800:].mean()])
    print('max values')
    print([dis_logloss.max(), gen_l1.max(),
           gen_logloss.max(), gen_loss.max()])


def difference_image(path, image, savename, scale=1):

    image_path = os.path.join('generator_results_archive', path, image)

    im = Image.open(image_path, mode='r').convert('L')

    im_arr = np.array(im)

    input_arr = im_arr[0:, 0:256]
    output_arr = im_arr[0:, 256:256*2]

    # diff_arr = np.abs(input_arr - output_arr)
    # diff_im = Image.fromarray(diff_arr)

    input_im = Image.fromarray(input_arr)
    output_im = Image.fromarray(output_arr)

    diff_im = ImageChops.difference(input_im, output_im)

    diff_np = np.array(diff_im)

    diff_np_norm = ((diff_np / diff_np.max()) * 255).astype('uint8')

    print(diff_np.dtype)
    print(diff_np.max())

    diff_total_np = np.concatenate(
        (input_arr, output_arr, diff_np, diff_np_norm), axis=1)

    fig = plt.figure(figsize=(20, 15))
    plt.imshow(diff_total_np, cmap='gray')
    plt.title('Diff image')
    # plt.savefig(os.path.join(path, savename))
    plt.show()


if __name__ == '__main__':

    path = os.path.join('data processing')
    filename = 'image aligned.png'
    savename = 'asdf.png'

    network_path = os.path.join(
        'mr_resoncstruction', 'results_MR2MR_M04-D19_h08-m55-s15')

    # get_l1(network_path)

    difference_image(network_path, 'test_epoch_100_batch_8.png',
                     'test_epoch_100_batch_8_diff.png', scale=1)
